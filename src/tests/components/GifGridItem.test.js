import { shallow } from "enzyme";
import { GifGridItem } from "../../components/GifGridItem";

describe('pruebas del componente <GifGridItem>', () => { 

    const title = 'Cualquier Titulo';
    const url = 'https://dsadasdsasa.com';

    const wrapper = shallow(<GifGridItem title={title} url={url} />);

    test('debe mostrar el componente correctamente', () => { 

        expect(wrapper).toMatchSnapshot();

     })

     test('debe de tener un parrafo con el title', () => { 

        const p = wrapper.find('p');
        expect(p.text().trim()).toBe(title);

      })

    test('debe de tener la imagen igual al url y alt de los props', () => { 

        const img = wrapper.find('img');
        // console.log(img.prop('src'));
        expect(img.prop('src')).toBe(url);
        expect(img.prop('alt')).toBe(title);

     })

     test('debe tener animate__fadeIn', () => { 

        const div = wrapper.find('div');
        const className = div.prop('className');

        expect(className.includes('animate__fadeIn')).toBe(true);
        

      })

 })