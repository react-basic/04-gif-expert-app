import { GifGrid } from "../../components/GifGrid";
import { shallow } from "enzyme";
import { useFetchGifs } from '../../hooks/useFetchGifs';

jest.mock('../../hooks/useFetchGifs');


describe('Pruebas componente <GifGrid />', () => { 
    
    const category = 'DBZ';

    test('debe de generar el componente GifGrid correctamente', () => { 

        useFetchGifs.mockReturnValue ({
            data: [],
            loading: true
        });

        const wrapper = shallow(<GifGrid category={category} />);

        expect(wrapper).toMatchSnapshot();

    });

    test('debe de mostrar items cuando se cargan imagenes useFetch', () => {
        
        const gifs = [{
                id: 'ABC',
                url: 'https://localhost/adsadsa.jpg',
                title: 'Cualquier cosa'
            },
            {
                id: 'ABC',
                url: 'https://localhost/adsadsa.jpg',
                title: 'Cualquier cosa'
            }
        ]

        useFetchGifs.mockReturnValue ({
            data: gifs,
            loading: false
        });

        const wrapper = shallow(<GifGrid category={category} />);

        expect(wrapper.find('p').exists()).toBe(false);
        expect(wrapper.find('GifGridItem').length).toBe(gifs.length);

    });


})